﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(GetById(id));
        }

        public T GetById(Guid id)
        {
            return Data.FirstOrDefault(x => x.Id == id);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var item = Data.FirstOrDefault(x => x.Id == id);
            return Task.FromResult(Data.Remove(item));
        }

        public Task<T> AddAsync(T item)
        {
            Data.Add(item);
            return GetByIdAsync(item.Id);
        }

        public Task<T> UpdateAsync(T item)
        {
            var oldItem = GetById(item.Id);
            int index = Data.IndexOf(oldItem);
            if (index != -1)
                Data[index] = item;
            return GetByIdAsync(item.Id);
        }
    }
}